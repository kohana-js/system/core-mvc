# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.5.0](https://gitlab.com/kohanajs/core-mvc/compare/v3.4.1...v3.5.0) (2023-06-06)


### Features

* expose language, clientIP, hostname to state ([67bf2c5](https://gitlab.com/kohanajs/core-mvc/commit/67bf2c553e0cdba638b3d4fb1494b93ca48eaacc))

### [3.4.1](https://gitlab.com/kohanajs/core-mvc/compare/v3.4.0...v3.4.1) (2023-02-21)


### Bug Fixes

* fix tests ([8604e45](https://gitlab.com/kohanajs/core-mvc/commit/8604e45f7ae008a37955094bad7c99607e9acf3a))

## [3.4.0](https://gitlab.com/kohanajs/core-mvc/compare/v3.3.0...v3.4.0) (2023-02-21)


### Features

* checkpoint for navigation back ([a1efd00](https://gitlab.com/kohanajs/core-mvc/commit/a1efd00d73c8351aa272ca91d514360a30eb5d62))

## [3.3.0](https://gitlab.com/kohanajs/core-mvc/compare/v3.2.2...v3.3.0) (2022-09-16)


### Features

* add controller onExit ([4e9e203](https://gitlab.com/kohanajs/core-mvc/commit/4e9e203082c220fca03d2c338bfa91eca933b075))

### [3.2.2](https://gitlab.com/kohanajs/core-mvc/compare/v3.2.1...v3.2.2) (2022-07-01)

### [3.2.1](https://gitlab.com/kohanajs/core-mvc/compare/v3.2.0...v3.2.1) (2022-06-22)

## [3.2.0](https://gitlab.com/kohanajs/core-mvc/compare/v3.1.0...v3.2.0) (2022-01-24)


### Features

* add language support by request.query ([642bdaa](https://gitlab.com/kohanajs/core-mvc/commit/642bdaa79ecde28edc23b878b8018a88a962ba58))

## 3.1.0 (2021-10-19)

## [3.1.0] - 2021-10-12
### Added
- add header X-Content-Type-Options with value nosniff;
- add full_action_name to state;
- controller redirect support forward query string

## [3.0.4] - 2021-09-07
### Added
- create CHANGELOG.md